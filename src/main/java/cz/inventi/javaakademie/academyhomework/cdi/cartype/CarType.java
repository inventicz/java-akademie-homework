package cz.inventi.javaakademie.academyhomework.cdi.cartype;

/**
 *
 * @author Tomas Poledny <tomas.poledny at inventi.cz>
 */
public interface CarType {

    String getName();

    int getVersion();

}
